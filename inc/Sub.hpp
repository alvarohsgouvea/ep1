#ifndef SUB_HPP
#define SUB_HPP

#include <string>
#include <vector>
#include "Barco.hpp"

using namespace std;

class Submarino : public Barco{
	private:
		char *matriz;
		int vida;
	public:
	Submarino(int x, int y, string direct, char *matriz, int number);

	Submarino();

	~Submarino();

	void set_matriz(char *matriz);

	void set_barco(int x, int y, string direct, int size);

	int dano(char *matriz, int coord1, int coord2);
};

#endif