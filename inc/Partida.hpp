#include <iostream>
#include <fstream>
#include "AI.hpp"

using namespace std;

void partida(int choice, Mapa *mapa1, Mapa *mapa2);

void menu(Mapa *mapa1, Mapa *mapa2){
	int choice;

	system("clear");
	cout<<"__________          __    __  .__                .__    .__              \n";
	cout<<"\\______   \\_____  _/  |__/  |_|  |   ____   _____|  |__ |__|_____  ______\n";
	cout<<"  |    |  _/\\__  \\\\   __\\   __\\  | _/ __ \\ /  ___/  |  \\|  \\____ \\/  ___/\n";
	cout<<"  |    |   \\ / __ \\|  |  |  | |  |_\\  ___/ \\___ \\|   Y  \\  |  |_> >___ \\ \n";
	cout<<"  |______  /(____  /__|  |__| |____/\\___  >____  >___|  /__|   __/____  >\n";
	cout<<"         \\/      \\/                     \\/     \\/     \\/   |__|      \\/ \n";
	cout<<endl<<endl;
	cout<<"  1 Man vs Machine (Em breve)"<<endl;
	cout<<"  2 PVP"<< endl;
	cout<<"  3 Instruncoes"<<endl;
	cout<<"  4 Sair"<<endl;
	cout<<"> ";
	cin>>choice;
	if(choice < 4)
		partida(choice, mapa1, mapa2);
	else
		exit(0);
}

void print_all(int hit, int player){
	string map1 = {"doc//mapa1.txt"}, map2 = {"doc//mapa2.txt"}; 
	char mapa1[13][13], mapa2[13][13];
	int i, j;

	ifstream play1;
	play1.open(map1);
	for(i = 0; i < 13; i++){
		for(j = 0; j < 13; j++){
			play1>>mapa1[i][j];
			if(mapa1[i][j] != '~' && mapa1[i][j] != '#' && mapa1[i][j] != 'X' && mapa1[i][j] != '*'){
				mapa1[i][j] = '~';
			}
		}
	}
	play1.close();

	ifstream play2;
	play2.open(map2);
	for(i = 0; i < 13; i++){
		for(j = 0; j < 13; j++){
			play2>>mapa2[i][j];
			if(mapa2[i][j] != '~' && mapa2[i][j] != '#' && mapa2[i][j] != 'X' && mapa2[i][j] != '*'){
				mapa2[i][j] = '~';
			}
		}
	}
	play2.close();
	system("clear");
	cout<<"   A  B  C  D  E  F  G  H  I  J  K  L  M\t";
	cout<<"   A  B  C  D  E  F  G  H  I  J  K  L  M\n1  ";
	for(i = 0; i < 13; i++){
		for(j = 0; j < 27; j++){
			if(j < 13)
				cout<<mapa1[i][j]<<"  ";
			if(j == 13){
				cout<<"\t";
				if(i < 9)
					cout<<i+1<<"  ";
				if(i > 8 && i < 13)
					cout<<i+1<<" ";
			}
			if(j > 13)
				cout<<mapa2[i][j-14]<<"  ";
		}
		cout<<endl;
		if(i < 8)
			cout<<i+2<<"  ";
		if(i > 7 && i < 12)
			cout<<i+2<<" ";
	}

	if(hit == -1)
		cout<<"Jogador "<<player<<" voce errou!\n";
	if(hit < 4 && hit > 0)
		cout<<"Jogador "<<player<<" voce acertou uma embarcacao!\n";
	if(hit == 0)
		cout<<"Jogador "<<player<<" voce afundou uma embarcacao!\n";
	if(hit == 7)
		cout<<"Jogador "<<player<<" seu missil foi desarmado!\n";
}
int compara(char letra){
	char ordem[14] = {"ABCDEFGHIJKLM"};
	int pos;

	for(int i = 0; i < 13; i++){
		if(toupper(letra) == ordem[i])
			pos = i;
	}

	return pos;
}

void partida(int choice, Mapa *mapa1, Mapa *mapa2){
	char buffer[2], letra[2];
	int x, y, hit, winner, *point, coord[2], level, AI_Buffer[5] = {-1, 0, 0, 0, 0};

	if(choice == 1){
		system("clear");
		cout<<"__________          __    __  .__                .__    .__              \n";
		cout<<"\\______   \\_____  _/  |__/  |_|  |   ____   _____|  |__ |__|_____  ______\n";
		cout<<"  |    |  _/\\__  \\\\   __\\   __\\  | _/ __ \\ /  ___/  |  \\|  \\____ \\/  ___/\n";
		cout<<"  |    |   \\ / __ \\|  |  |  | |  |_\\  ___/ \\___ \\|   Y  \\  |  |_> >___ \\ \n";
		cout<<"  |______  /(____  /__|  |__| |____/\\___  >____  >___|  /__|   __/____  >\n";
		cout<<"         \\/      \\/                     \\/     \\/     \\/   |__|      \\/ \n";
		cout<<endl<<endl;
		cout<<"  1 Facil"<<endl;
		cout<<"  2 Medio"<< endl;
		cout<<"  3 Dificil"<<endl;
		cout<<"  4 Sair"<<endl;
		cout<<"> ";
		if(level == 4){
			exit(0);
		}
		cin>>level;
		system("clear");
		cout<<"Jogador 1 esse eh seu mapa:\n";
		mapa1->imprime_mapa();
		fflush(stdin);
		cout<<"Pressione enter para continuar...\n";
		scanf("%c%c", &buffer[0], &buffer[1]);
		print_all(99, 1);
		do{
			winner = 1;
			cout<<"Jogador 1:\n";
			cout<<"> ";
			fflush(stdin);
			cin>>letra[0]>>y;
			y--;
			x = compara(letra[0]);
			hit = mapa2->ataque(y, x);
			print_all(hit, 1);
			cout<<"Pressione enter para continuar...\n";
			scanf("%c%c", &buffer[0], &buffer[1]);
			if((mapa2->qtd)-(mapa2->derrubados) != 0){
				winner = 2;
				point = AI_ataque(level, coord, AI_Buffer);
				x = *point;
				y = *(point+1);
				hit = mapa1->ataque(y, x);
				AI_Buffer[0] = hit;
				AI_Buffer[3] = AI_Buffer[1];
				AI_Buffer[4] = AI_Buffer[2];
				AI_Buffer[1] = x;
				AI_Buffer[2] = y;
				print_all(hit, 2);	
			}
		}while(((mapa1->qtd)-(mapa1->derrubados) != 0) || ((mapa2->qtd)-(mapa2->derrubados) != 0));
		cout<<"Jogador "<<winner<<" voce ganhou!\n";
	}
	if(choice == 2){
		system("clear");
		cout<<"Jogador 1 esse eh seu mapa:\n";
		mapa1->imprime_mapa();
		fflush(stdin);
		cout<<"Pressione enter para continuar...\n";
		scanf("%c%c", &buffer[0], &buffer[1]);
		system("clear");
		cout<<"Jogador 2 esse eh seu mapa:\n";
		mapa2->imprime_mapa();
		fflush(stdin);
		cout<<"Pressione enter para continuar...\n";
		scanf("%c", &buffer[0]);
		print_all(99, 1);
		do{
			winner = 1;
			cout<<"Jogador 1:\n";
			cout<<"> ";
			fflush(stdin);
			cin>>letra[0]>>y;
			y--;
			x = compara(letra[0]);
			hit = mapa2->ataque(y, x);
			print_all(hit, 1);
			if((mapa2->qtd)-(mapa2->derrubados) != 0){
				winner = 2;
				cout<<"Jogador 2:\n";
				cout<<"> ";
				fflush(stdin);
				cin>>letra[1]>>y;
				y--;
				x = compara(letra[1]);
				hit = mapa1->ataque(y, x);
				print_all(hit, 2);	
			}
		}while(((mapa1->qtd)-(mapa1->derrubados) != 0) || ((mapa2->qtd)-(mapa2->derrubados) != 0));
		cout<<"Jogador "<<winner<<" voce ganhou!\n";
	}
	if(choice == 3){
		cout<<"Objetivo:\n\tCada jogador possui uma quantidade de barcos que o outro jogador deve tentar afundar\n\nComo Jogar:\n\tOs jogadores revezarao em turnos digitando coordenadas para serem atacadas, a cada jogada sera retornado um feedback sobre como foi o sucesso do seu ataque,\n\tlembrando que cada tipo de embarcacao possui uma caracteristica propia\n\nEmbarcacoes:\n\tCanoa:\n\t\tEmbarcacao mais basica posssui apenas uma coordenada e nenhuma habilidade especial\n\tSubmarino:\n\t\tPossui duas coordenadas (dois espacos) e sao nescessarios dois ataques para derrubar cada coordenada\n\tPorta-Avioes:\n\t\tMaior embarcacao posssuindo 4 espacos e a habilidade especial de poder desarmar o ataque na coordenada"<<endl;
		cout<<"\nObservacoes: Os ataques deverao ser informados na ordem Letra (maiuscula ou minuscula) e entao numero\n\tex: M12"<<endl;
		fflush(stdin);
		cout<<"Pressione enter para continuar...\n";
		scanf("%c%c", &buffer[0], &buffer[1]);
		menu(mapa1, mapa2);
	}
}

void End_Game(){
	remove("doc//mapa1.txt");
	remove("doc//mapa2.txt");
}