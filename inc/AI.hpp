#include <iostream>
#include <fstream>

using namespace std;

int *AI_ataque(int level, int *coord, int AI_Buffer[]){
	ifstream arq;
	char local[13][13];
	int i;

	if(level == 1 || AI_Buffer[0] == -1){
		*coord = rand()%13;
		*(coord+1) = rand()&13;		
	}else{
		arq.open("doc//mapa1.txt");
		for(int i = 0; i < 13; i++){
			for(int j = 0; j < 13; j++){
				arq>>local[i][j];
			}
		}
		arq.close();
		do{
			*coord = rand()%13;
			*(coord+1) = rand()&13;
		}while(local[*coord][*(coord+1)] != 'X' && local[*coord][*(coord+1)] != '#');
	}
	if(level == 2){
		if(AI_Buffer[0] > 0 && AI_Buffer[0] < 4 && local[AI_Buffer[1]][AI_Buffer[2]] == '#'){
			if(local[AI_Buffer[1]][AI_Buffer[2]] == '#'){
				if(local[AI_Buffer[3]][AI_Buffer[4]] == '#'){
					i = 2;
					do{
					*coord = AI_Buffer[1] + (i*(AI_Buffer[3]-AI_Buffer[1]));
					*(coord+1) = AI_Buffer[2] + (i*(AI_Buffer[4]-AI_Buffer[2]));
					i++;
					}while(local[*coord][*(coord+1)] == '#');
				}else{
					i = 2;
					do{
					*coord = AI_Buffer[1] - (i*(AI_Buffer[3]-AI_Buffer[1]));
					*(coord+1) = AI_Buffer[2] - (i*(AI_Buffer[4]-AI_Buffer[2]));
					i++;
					}while(local[*coord][*(coord+1)] == '#');	
				}
			}
		}else if(AI_Buffer[0] > 0 && AI_Buffer[0] < 4){
			do{
				*coord = rand()%13;
				*(coord+1) = rand()&13;
			}while(local[*coord][*(coord+1)] != 'X' && local[*coord][*(coord+1)] != '#');
		}
		if(AI_Buffer[0] == 7 || local[AI_Buffer[1]][AI_Buffer[2]] == '*'){
			*coord = AI_Buffer[1];
			*(coord+1) = AI_Buffer[2];
		}
		if(AI_Buffer[0] == 0){
			do{
				*coord = rand()%13;
				*(coord+1) = rand()&13;
			}while(local[*coord][*(coord+1)] != 'X' && local[*coord][*(coord+1)] != '#');
		}
	}

	return coord;
}
