#ifndef CANOA_HPP
#define CANOA_HPP

#include <string>
#include <vector>
#include "Barco.hpp"

using namespace std;

class Canoa : public Barco{
	private:
		char *matriz;
		int vida;
	public:
	Canoa(int x, int y, string direct, char *matriz, int number);

	Canoa();

	~Canoa();

	void set_matriz(char *matriz);

	void set_barco(int x, int y, string direct, int size);

	int dano(char *matriz, int coord1, int coord2);
};

#endif