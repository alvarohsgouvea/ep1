#ifndef BARCO_HPP
#define BARCO_HPP

#include <string>

using namespace std;

class Barco {
	private:
		int x;
		int y;
		int size;
		int number;
		string direct;
	public:

		Barco(int x, int y, int size, string direct);
		Barco();
		~Barco();

		int get_x();
		void set_x(int x);
		int get_y();
		void set_y(int y);
		int get_size();
		void set_size(int size);
		string get_direct();
		void set_direct(string direct);
		void set_number(int number);


		virtual void set_barco(int x, int y, string direct, int size);
		virtual int dano(char *matriz, int coord1, int coord2);
};

#endif