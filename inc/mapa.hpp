#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>
#include <vector>
#include "Barco.hpp"

using namespace std;

class Mapa {
	private:
		string nome;
	public:

	Mapa();

	~Mapa();

	vector <Barco*> barco;
	vector<int> X;
	vector<int> Y;
	vector<int> XF;
	vector<int> YF;
	string map;
	int qtd;
	int derrubados;
	int atualiza(int Num, int Letra);
	string get_nome();
	void set_nome(string map);
	void define_mapa(char player);
	void set_barco(int x, int y, string direct, string type, char *matriz, int atual);
	void set_qtd();
	void set_derrubados();

	//Outros metodos
	void imprime_mapa();
	int ataque(int coord1, int coord2);
};

#endif