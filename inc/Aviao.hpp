#ifndef AVIAO_HPP
#define AVIAO_HPP

#include <string>
#include <vector>
#include "Barco.hpp"

using namespace std;

class Aviao : public Barco{
	private:
		char *matriz;
		int vida;
	public:
	Aviao(int x, int y, string direct, char *matriz, int number);

	Aviao();

	~Aviao();

	void set_matriz(char *matriz);

	void set_barco(int x, int y, string direct, int size);

	int dano(char *matriz, int coord1, int coord2);
};

#endif