#include <iostream>
#include "Sub.hpp"
#include "Barco.hpp"
using namespace std;

Submarino::Submarino(int x, int y, string direct, char *matriz, int number){
	set_x(x);
	set_y(y);
	set_direct(direct);
	set_matriz(matriz);
	set_size(2);
	set_number(number);
	vida = 4;
}

Submarino::Submarino(){
}

Submarino::~Submarino(){	
}

void Submarino::set_matriz(char *matriz){
	this->matriz = matriz;
}

void Submarino::set_barco(int x, int y, string direct, int size){
	int xF, yF;

	if(direct == "cima"){
		xF = x;
		x = x-size+1;
		yF = y;
	}
	if(direct == "baixo"){
		xF = x+size-1;
		yF = y;
	}
	if(direct == "esquerda"){
		yF = y;
		y = y-size+1;
		xF = x;
	}
	if(direct == "direita"){
		yF = y+size-1;
		xF = x;
	}
	for(int i = x; i <= xF; i++){
		for (int j = y; j <= yF; j++)
		{
			*(matriz+i*13+j) = 'S';
		}
	}
}

int Submarino::dano(char *matriz, int coord1, int coord2){
	if(*(matriz+coord1*13+coord2) != 'X'){
		vida--;
		return vida;
	}else{
		return -1;
	}
}