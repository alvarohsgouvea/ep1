#include <fstream>
#include <iostream>
#include <time.h>
#include <vector>
#include "Mapa.hpp"
#include "Partida.hpp"
using namespace std;

void set_mapa(Mapa *mapa, string nome, char player){
	mapa->set_nome(nome);
	mapa->define_mapa(player);
}

int main(){
	Mapa mapa1;
	Mapa mapa2;
	srand(time(NULL));
	//Configuracoes iniciais
	set_mapa(&mapa1, "doc//mapa1.txt", '1');
	set_mapa(&mapa2, "doc//mapa2.txt", '2');

	//Partida
	menu(&mapa1, &mapa2);

	//Configuracoes finais
	End_Game();
}