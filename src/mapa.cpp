#include "Mapa.hpp"
#include "Barco.hpp"
#include "Aviao.hpp"
#include "Sub.hpp"
#include "Canoa.hpp"
#include <fstream>
#include <iostream>
#include <vector>
#include <time.h>
#include <string>
using namespace std;

Mapa::Mapa(){
	cout<<"Mapa Inicializado!\n"<<endl;
}

Mapa::~Mapa(){
	cout<<"Mapa excluido!\n"<<endl;
}

string Mapa::get_nome(){
	return map;
}

void Mapa::set_nome(string map){
		this->map = map;
}
void Mapa::set_qtd(){
	this->qtd = -1;
}
void Mapa::set_derrubados(){
	this->derrubados = 0;
}

void Mapa::set_barco(int x, int y, string direct, string type, char *matriz, int atual){
	int xF = x, yF = y, size = 1;
	qtd++;
	if(type == "canoa"){
		barco.push_back(new Canoa(x, y, direct, matriz, atual));
		barco[atual]->set_barco(x, y, direct, 1);
	}
	if(type == "submarino"){
		barco.push_back(new Submarino(x, y, direct, matriz, atual));
		barco[atual]->set_barco(x, y, direct, 2);
		size = 2;
	}
	if(type == "porta-avioes"){
		barco.push_back(new Aviao(x, y, direct, matriz, atual));
		barco[atual]->set_barco(x, y, direct, 4);
		size = 4;
	}
	if(direct == "cima")
		x -= (size-1);
	if(direct == "baixo")
		xF += (size-1);
	if(direct == "direita")
		yF += (size-1);
	if(direct == "esquerda")
		y -= (size-1);

	X.push_back(x);
	Y.push_back(y);
	XF.push_back(xF);
	YF.push_back(yF);
}

void Mapa::define_mapa(char player){
	string data, barco, direct, jogador = {"player_"};
	int coord1, coord2, atual = 0;
	char local[13][13], dataset[2], *p;

	set_qtd();
	set_derrubados();

	for(int i = 0; i < 13; i++){
		for(int j = 0; j < 13; j++){
			local[i][j] = '~';
		}
	}

	jogador = jogador+player;

	p = &local[0][0];
	
	ifstream ptr;
	ptr.open("doc//map_1.txt");
		do{
			ptr>>data;
		}while(data != jogador);
		do{
			ptr>>dataset;
			coord1 = atoi(dataset);
			ptr>> coord2;
			ptr>>barco;
			ptr>> direct;
			if(dataset[0] != '#'){
				set_barco(coord1, coord2, direct, barco, p, atual);
				atual++;
			}
		}while(dataset[0] != '#');
	ptr.close();
	ofstream arq;

	arq.open(map);
	for(int i = 0; i < 13; i++){
		for(int j = 0; j < 13; j++){
			arq<<local[i][j];
		}
		arq<<endl;
	}
	arq.close();
}

void Mapa::imprime_mapa(){
	int i, j;
	char local[13][13];
	ifstream arq;

	arq.open(map);
		for(i = 0; i < 13; i++){
			for(j = 0; j < 13; j++){
				arq>>local[i][j];
			}
		}
	arq.close();

	cout<<"   A  B  C  D  E  F  G  H  I  J  K  L  M\n1  ";
	for(i = 0; i < 13; i++){
		for(j = 0; j < 13; j++){
			cout<<local[i][j]<<"  ";
		}
		cout<<endl;
		if(i < 8)
			cout<<i+2<<"  ";
		if(i > 7 && i < 12)
			cout<<i+2<<" ";
	}
	cout<<"_________________"<<endl;
}

int Mapa::ataque(int coord1, int coord2){
	ifstream ptr;
	ofstream arq;
	int pos = -1, hit = -1;
	char local[13][13], type;

	ptr.open(map);
		for(int i = 0; i < 13; i++){
			for(int j = 0; j < 13; j++){
				ptr>>local[i][j];
			}
		}
	ptr.close();
	for(int i = 0; i < qtd; i++){
		for(int j = X[i]; j <= XF[i]; j++){
			for(int k = Y[i]; k <= YF[i]; k++){
				if(coord1 == j && coord2 == k){
					pos = i;
					type = local[j][k];
				}
			}
		}
	}
	if(pos != -1){
		hit = barco[pos]->dano(*local, coord1, coord2);
	}
	if(hit < 4 && hit != -1 && pos != -1 && type != 'S'){
		local[coord1][coord2] = '#';
	}
	if(type == 'S'){
		if(local[coord1][coord2] == 'S' && pos != -1){
			local[coord1][coord2] = '*';
		}else if(local[coord1][coord2] == '*' && pos != -1){
			local[coord1][coord2] = '#';
		}
	}
	if(hit == -1 && local[coord1][coord2] != '#'){
		local[coord1][coord2] = 'X';
	}else{
		derrubados++;
	}
	arq.open(map);
		for(int i = 0; i < 13; i++){
			for(int j = 0; j < 13; j++){
				arq<<local[i][j];
			}
			arq<<endl;
		}
	arq.close();
	return hit;
}