#include <iostream>
#include "Aviao.hpp"
#include "Barco.hpp"
using namespace std;

Aviao::Aviao(int x, int y, string direct, char *matriz, int number){
	set_x(x);
	set_y(y);
	set_direct(direct);
	set_matriz(matriz);
	set_size(2);
	set_number(number);
	vida = 4;
}

Aviao::Aviao(){
}

Aviao::~Aviao(){	
}

void Aviao::set_matriz(char *matriz){
	this->matriz = matriz;
}

void Aviao::set_barco(int x, int y, string direct, int size){
	int xF, yF;

	if(direct == "cima"){
		xF = x;
		x = x-size+1;
		yF = y;
	}
	if(direct == "baixo"){
		xF = x+size-1;
		yF = y;
	}
	if(direct == "esquerda"){
		yF = y;
		y = y-size+1;
		xF = x;
	}
	if(direct == "direita"){
		yF = y+size-1;
		xF = x;
	}
	for(int i = x; i <= xF; i++){
		for (int j = y; j <= yF; j++)
		{
			*(matriz+i*13+j) = 'P';
		}
	}
}
int Aviao::dano(char *matriz, int coord1, int coord2){
	int N;
	N = rand()%2;
	if(*(matriz+coord1*13+coord2) != '#'){
		if(N == 0){
			vida--;
			return vida;
		}else{
			return 7;
		}
	}else{
		return -1;
	}
	return 0;
}